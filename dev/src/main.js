import Vue from 'vue'

import Vuetify from 'vuetify'
Vue.use(Vuetify);

import $ from 'jquery'
window.$ = window.jQuery = $;

import 'vuetify/dist/vuetify.css'
import './app.css'

window.EventBus = new Vue();

import App from './App'


const app = new Vue({
    render: h => h(App),
    vuetify: new Vuetify()
}).$mount('#app');
